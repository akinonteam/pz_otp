import Inputmask from 'inputmask';

class PzOTP {
  /**
   * Settings object (destructured parameters)
   * @typedef {Object} Settings
   * @property {String} formSelector Selector for form wrapper, required, has no defaults
   * @property {String} [actionForm=null] The instance of ActionForm, set it only if using ActionForm
   * @property {String} [requestURL=location.pathname] URL to submit the form to
   * @property {String} [requestMethod=POST] Request method to use when submitting the form
   * @property {String} [modalSelector=.js-otp-modal] Selector for modal wrapper
   * @property {String} [submitSelector=.js-otp-submit] Selector for submit button
   * @property {String} [codeSelector=.js-otp-code] Selector for code input
   * @property {String} [countdownSelector=.js-otp-countdown] Selector for countdown element
   * @property {String} [errorSelector=.js-otp-error] Selector for error displaying element
   * @property {String} [resubmitText=Yeni Kod Al] Text to show in button when user needs to request a new code
   * @property {Number} [timer=60] Time before user needs to request a new code, in seconds
   * @property {Number} [codeLength=6] Length of expected OTP code
   * @property {Object} [headerParams={}] Set header params on request
   * @property {Boolean} [contentTypeForm=null] Set content-type on request
   * @property {Function} [onBeforeSubmit=null] Function to run before submitting the form
   * @property {Function} [onModalOpen=null] Function to run after modal opens
   * @property {Function} [onModalClose=null] Function to run after modal closes
   */
  /**
   * An instance of PZ OTP module
   * @param {Settings} settings An object containing settings for the instance
   */
  constructor({
    formSelector,
    actionForm = null,
    requestURL = window.location.pathname,
    requestMethod = 'POST',
    modalSelector = '.js-otp-modal',
    submitSelector = '.js-otp-submit',
    codeSelector = '.js-otp-code',
    countdownSelector = '.js-otp-countdown',
    errorSelector = '.js-otp-error',
    resubmitText = 'Yeni Kod Al',
    timer = 60,
    codeLength = 6,
    headerParams = {},
    attributeParams = ['interest'],
    onBeforeSubmit = null,
    contentTypeForm = null,
    onModalOpen = null,
    onModalClose = null
  }) {
    this.nodes = {
      form: $(formSelector),
      modal: $(modalSelector),
      submit: $(submitSelector),
      otpCode: $(codeSelector),
      countdown: $(countdownSelector),
      error: $(errorSelector)
    }

    this.callbacks = {
      onBeforeSubmit,
      onModalOpen,
      onModalClose,
    }

    this.actionForm = actionForm;
    this.requestURL = requestURL;
    this.requestMethod = requestMethod;
    this.timer = timer;
    this.contentTypeForm = contentTypeForm;
    this.codeLength = codeLength;
    this.headerParams = headerParams;
    this.attributeParams = attributeParams;
    this.countdown = null;
    this.submitText = this.nodes.submit.text();
    this.resubmitText = resubmitText;
    this.data = '';
    this.code = '';
    this.resend = false;

    this.init();
  }

  /**
   * Runs mandatory functions for this module to work
   */
  init() {
    this.clearInstance(true);
    this.appyInputmask();

    this.nodes.submit.on('click', (e) => {
      e.preventDefault();
      this.handleCodeSubmit();
    });
  }

  appyInputmask() {
    new Inputmask({
      mask: '9',
      repeat: this.codeLength
    }).mask(this.nodes.otpCode[0]);
  }

  /**
   * Goes through each input in given form and creates a JSON with their names and values
   * @returns {Object}
   */
  getFormData() {
    const dataArr = this.nodes.form.serializeArray();
    const dataObj = {};

    for (let data of dataArr) {
      const input = this.nodes.form.find(`input[name="${data.name}"]`)[0];

      if (this.attributeParams.includes(input.name)) {
        dataObj.attributes = dataObj.attributes || {};
        dataObj.attributes[data.name] = data.value;
      } else {
        dataObj[data.name] = input && input.inputmask
          ? input.inputmask.unmaskedvalue()
          : data.value;
      }
    }

    return dataObj;
  }

  /**
   * Handles behaviour for code validations
   * @returns {Boolean}
   */
  validateCode() {
    return this.nodes.otpCode.val() ? true : false;
  }

  handleInvalidCode() {
    this.nodes.otpCode.focus();
  }

  /**
   * Submits the form with collected data and executes behaviour based on the response
   * @returns {Promise}
   */
  async submitRequest() {
    this.data = this.getFormData();
    this.code = this.nodes.otpCode.val();

    if (this.code && !this.resend) {
      this.data['code'] = this.code;
    } else if (!this.code && this.resend) {
      this.data['resend'] = true;
    }

    return new Promise((resolve, reject) => {
      $.ajax({
        method: this.requestMethod,
        url: this.requestURL,
        data: this.contentTypeForm ? this.data : JSON.stringify(this.data),
        contentType: this.contentTypeForm
          ? 'application/x-www-form-urlencoded'
          : 'application/json',
        headers: this.headerParams,
        beforeSend: () => {
          if (this.callbacks.onBeforeSubmit) {
            this.callbacks.onBeforeSubmit()
          }
        },
        success: (response, status, xhr) => {
          this.toggleLoader(false);

          if (xhr.status === 202) {
            this.toggleModal(true);
            this.startCountdown(true);

            return resolve({ resultCode: 202 });
          }

          if (this.actionForm) {
            return this.actionForm.onSuccess({
              data: response,
              status,
              xhr,
              resultCode: 201
            });
          }

          resolve({
            data: response,
            status,
            xhr,
            resultCode: 201
          });
        },
        error: (errors) => {
          this.toggleLoader(false);
          this.handleSubmitError(errors);
          reject(errors);
        },
      });
    });
  }

  /**
   * Handles behavior for submitting the form internally
   */
  handleCodeSubmit() {
    if (!this.resend && !this.validateCode()) {
      this.handleInvalidCode();
      return false;
    }

    this.nodes.error.addClass('d-none');
    this.toggleLoader(true);
    this.submitRequest(this.resend);
  }

  /**
   * Clears existing data and either displays an error returned from backend
   * or a generic error defined in template
   * @param {Object} errors Object returned from an $.ajax request which resulted with an error
   */
  handleSubmitError(errors) {
    this.clearInstance();

    if (errors.status == 406) {
      return this.nodes.error
        .text(errors.responseJSON.non_field_errors)
        .removeClass('d-none');
    }

    return this.nodes.error.removeClass('d-none');
  }

  /**
   * Clears saved data, code and optionally, the text and value of code input
   * @param {Boolean} input Whether to clean text and value of code input
   */
  clearInstance(input = false) {
    this.data = '';
    this.code = '';

    this.nodes.error.addClass('d-none');

    if (input) {
      this.nodes.otpCode.text('').val('');
    }
  }

  /**
   * Shows or hides the modal
   * @param {Boolean} status Whether to show or hide the modal
   */
  toggleModal(status) {
    if (status) {
      this.nodes.modal.modal('show');

      if (this.callbacks.onModalOpen) {
        this.callbacks.onModalOpen();
      }

      return;
    }

    if (this.callbacks.onModalClose) {
      return this.callbacks.onModalClose();
    }
  }

  /**
   * Enables or disables the loader on submit button
   * @param {Boolean} status Whether to enable or disable the loader on submit button
   */
  toggleLoader(status) {
    if (status) {
      return this.nodes.submit
        .addClass('-loading')
        .attr('disabled', true);
    }

    return this.nodes.submit
      .removeClass('-loading')
      .attr('disabled', false);
  }

  /**
   * Starts and displays a countdown with given timer, if there is no active countdown
   * @param {Boolean} force Whether to override the current countdown and start a new one
   */
  startCountdown(force) {
    if (this.countdown && !force) {
      return false;
    }

    if (this.countdown && force) {
      clearInterval(this.countdown);
    }

    let localTimer = this.timer;

    this.nodes.countdown.text(localTimer);
    this.nodes.submit.text(this.submitText);
    this.resend = false;

    this.countdown = setInterval(() => {
      localTimer--;
      this.nodes.countdown.text(localTimer);

      if (localTimer == 0) {
        clearInterval(this.countdown);

        this.countdown = null;
        this.resend = true;
        this.handleCountdownEnd();
      }
    }, 1000);
  }

  /**
   * Executes behaviour for when the countdown ends
   */
  handleCountdownEnd() {
    this.nodes.submit.text(this.resubmitText);
    this.clearInstance(true);
  }
}

export default PzOTP;
