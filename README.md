# pz_otp
​
This app enables SMS verification (OTP) support for desired actions,
such as registration and updating profile information.

It **does not** work with checkout pages due to them having
completely different architecture.

Backend configuration has to be done properly for this module to work.
For example, register request must respond with status code 202.

## Usage
### 1. Install the app

At project root, create a requirements.txt if it doesn't exist and add the following line to it;

```bash
-e git+https://git@bitbucket.org/akinonteam/pz_otp.git@15f3121#egg=pz_otp
```

Note: always make sure to use the latest commit id in the above example.
For more information about this syntax, check [pip install docs](https://pip.pypa.io/en/stable/reference/pip_install/#git).

Next, run the following command to install the package.

```bash
# in venv
pip install -r requirements.txt
```

### 2. Install the npm package

```bash
# in /templates
yarn add ​git+ssh://git@bitbucket.org:akinonteam/pz_otp.git#15f3121
```

Make sure to use the same git commit id as in `requirements.txt`.

### 3. Add to the project

```python
# omnife_base/settings.py:

INSTALLED_APPS.append('pz_otp')
```

### 4. Import template:

```jinja
{% from 'pz_otp/index.html' import OTPModal %}
​
{{ OTPModal() }}
```

### 5. Import and initialize JS

```js
import PzOTP from 'pz_otp';

// ...

new PzOTP({
  formSelector: '.js-otp-form'
});
```

### 6. Import styles:

```scss
@import "~pz_otp/";
```

## Usage with ActionForm

To integrate this module with current ActionForm structure, you will need to pass the actionForm as a setting and set `form.action` to `submitRequest` function from the OTP class.

```js
class RegisterForm {
  constructor($form) {
    this.form = actionForm($form);

    this.otp = new PzOTP({
      formSelector: $form,
      actionForm: this.form
    });

    this.form.action = this.otp.submitRequest.bind(this.otp);
  }

  // ...
}
```

Note the usage of `bind()`, this is required to make sure `this` keyword isn't referencing
`this.form` but instead `this.otp`.

Finally, make sure your `onSuccess` method properly handles the 202 status code.
If backend responds with 202, the request promise used in this module will also resolve
with a `resultCode` of 202. In this case, you don't want `onSuccess` to do anything, such as
redirection.

```js
// registerForm.js
  onSuccess(response) {
    // response parameter here comes from resolved promise of submitRequest() within pz_otp
    if (response.resultCode == 202) {
      // Don't trigger the onSuccess functionality because 202 is only for showing the OTP modal
      return false;
    }

    const queries = new URLSearchParams(window.location.search);
    const next = queries.get('next');

    if (next) {
      document.location.href = next;
      return;
    }

    pushEvent({
      type: actionTypes.signUpAction
    });
    document.location.reload();
  }
```

## Customizing the modal
### Default template
If all you need is a simple modal, then you can use the Jinja2 macro as it was shown before.

```jinja2
{{ OTPModal() }}
```

This will generate a Bootstrap 4 modal with header, content and footer sections.

![Default Modal](https://i.ibb.co/MNWMdXs/image.png)

It also has all the BEM selectors you would need for applying custom CSS.

### Custom template

If you want a custom structure to replace the header, content and footer sections of the modal, you need to use `call`.

```jinja
{% call OTPModal() %}
  <h2>My Custom Modal</h2>
  <label>
    Your code:
    <input type="text" name="code">
  </label>
  <button>Submit</button>
{% endcall %}
```
## Template settings

```jinja
{# Defaults #}
{{
  OTPModal(
    class='',
    title='SMS Doğrulama',
    description='Lütfen telefonunuza gelen SMS doğrulama kodunu giriniz',
    countdown_text='',
    button_text='Doğrula',
    error_text='SMS doğrulama başarısız')
}}
```
All of the following are optional parameters for the Jinja2 macro.

- **class**: (String) A general wrapper class for the template

- **title**: (String) Modal title shown on top

- **description**: (String) Description / explanation text shown above the input field

- **countdown_text**: (String) Text to be shown before the countdown

- **button_text**: (String) Text to be shown inside the button

- **error_text**: (String) Text to be shown when there is a general error. Note that this will be overriden by JS if there's an error there.

## JS settings

Pass an object, which will be taken as destructured parameters, to customize things on the JS class.

```js
// Defaults
new PzOTP({
  formSelector: 'required',
  actionForm: null,
  requestURL: window.location.pathname,
  requestMethod: 'POST',
  modalSelector: '.js-otp-modal',
  submitSelector: '.js-otp-submit',
  codeSelector: '.js-otp-code',
  countdownSelector: '.js-otp-countdown',
  errorSelector: '.js-otp-error',
  resubmitText: 'Yeni Kod Al',
  timer: 60,
  codeLength: 6,
  headerParams = {},
  contentTypeForm = null,
  onBeforeSubmit: null,
  onModalOpen: null,
  onModalClose: null
});
```

- **formSelector**: (String, *Required*) Selector for form wrapper

- **actionForm**: (ActionForm) The instance of ActionForm, if used

- **requestURL** (String) URL to submit the form to

- **requestMethod** (String) Request method to use when submitting the form

- **modalSelector** (String) Selector for modal wrapper

- **submitSelector** (String) Selector for submit button

- **codeSelector** (String) Selector for code input

- **countdownSelector** (String) Selector for countdown element

- **errorSelector** (String) Selector for error displaying element

- **resubmitText** (String) Text to show in button when user needs to request a new code

- **timer**: (Number) Time before user needs to request a new code in seconds

- **codeLength**: (Number) Length of expected OTP code, used for inputmask

- **headerParams**: (Object) Set header params on request

- **contentTypeForm**: (Boolean) Set content-type on request

- **onBeforeSubmit** Function to run before submitting the form

- **onModalOpen** Function to run after modal opens

- **onModalClose** Function to run after modal closes
